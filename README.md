# KO web pages
This repository contains web pages of [KO course][1].

It's built by [sphinx][2], using [reStructuredText][3]. See [Quick
reStructuredText][4] for quick overview.

[1]: http://ko.pages.fel.cvut.cz/
[2]: https://www.sphinx-doc.org/
[3]: https://docutils.sourceforge.io/rst.html
[4]: https://docutils.sourceforge.io/docs/user/rst/quickref.html
