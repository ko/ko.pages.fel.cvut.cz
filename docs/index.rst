.. KO web pages documentation master file, created by
   sphinx-quickstart on Fri Apr 29 12:49:55 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KO web pages's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   notebooks/Masterclass.ipynb
   notebooks/ILP_basics.ipynb
   notebooks/basic.ipynb

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
